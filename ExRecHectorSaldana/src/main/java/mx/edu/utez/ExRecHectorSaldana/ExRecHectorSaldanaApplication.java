package mx.edu.utez.ExRecHectorSaldana;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExRecHectorSaldanaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExRecHectorSaldanaApplication.class, args);
	}

}
