package mx.edu.utez.ExRecHectorSaldana.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class EmailConfig {
    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        // Datos del servidor
        mailSender.setHost("smtp.gmail.com"); // para Gmail
        mailSender.setPort(587);


        // Datos del usuario
        mailSender.setUsername("");  //Colocar cuenta de correo
        mailSender.setPassword("");  // Colocar contraseña del correo


        // Propiedades del servidor / conexión
        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp"); // Es el protocolo a utilizar para enviar
        props.put("mail.smtp.auth", "true"); //Autenticación
        props.put("mail.smtp.starttls.enable", "true"); // Activar tls un mecanismo que usa smtp, es la mejora de ssl
        props.put("mail.debug", "true"); // Mostrar en consola como realiza la conexión, email, y si hubo respuesta 200
        return mailSender;

    }
}
