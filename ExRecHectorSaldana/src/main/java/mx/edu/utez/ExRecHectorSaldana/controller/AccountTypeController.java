package mx.edu.utez.ExRecHectorSaldana.controller;

import mx.edu.utez.ExRecHectorSaldana.entity.AccountType;
import mx.edu.utez.ExRecHectorSaldana.service.AccountTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class AccountTypeController {

    @Autowired
    private AccountTypeService accountTypeService;

    @GetMapping("/tipo_cuenta")
    public List<AccountType> getAll(){
        return accountTypeService.getAll();
    }

    @GetMapping("/tipo_cuenta/{id}")
    public ResponseEntity<AccountType> getOne(@PathVariable("id")long id){
        return accountTypeService.getOne(id)
                .map(accountType -> new ResponseEntity<>(accountType, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/tipo_cuenta")
    public AccountType insertOne(@RequestBody AccountType accountType){
        return accountTypeService.insertOne(accountType);
    }

    @PutMapping("/tipo_cuenta")
    public AccountType updateOne(@RequestBody AccountType accountType){
        return accountTypeService.updateOne(accountType);
    }
    @DeleteMapping("/tipo_cuenta/{id}")
    public boolean deleteOne(@PathVariable("id")long id){
        return accountTypeService.deleteOne(id);
    }
}
