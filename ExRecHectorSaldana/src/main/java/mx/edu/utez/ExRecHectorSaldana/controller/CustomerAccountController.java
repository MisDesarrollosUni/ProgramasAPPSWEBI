package mx.edu.utez.ExRecHectorSaldana.controller;

import mx.edu.utez.ExRecHectorSaldana.entity.CustomerAccount;
import mx.edu.utez.ExRecHectorSaldana.service.CustomerAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CustomerAccountController {
    @Autowired
    private CustomerAccountService customerAccountService;

    @GetMapping("/cuentacliente")
    public List<CustomerAccount> getAll(){
        return customerAccountService.getAll();
    }

    @GetMapping("/cuentacliente/{id}")
    public ResponseEntity<CustomerAccount> getOne(@PathVariable("id") long id){
        return customerAccountService.getOne(id)
                .map(customerAccount -> new ResponseEntity<>(customerAccount, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/cuentacliente")
    public CustomerAccount insertOne(@RequestBody CustomerAccount customerAccount){
        return customerAccountService.insertOne(customerAccount);
    }

    @PutMapping("/cuentacliente")
    public CustomerAccount updateOne(@RequestBody CustomerAccount customerAccount){
        return customerAccountService.updateOne(customerAccount);
    }

    @DeleteMapping("/cuentacliente/{id}")
    public boolean deleteOne(@PathVariable("id") long id){
        return customerAccountService.deleteOne(id);
    }

}
