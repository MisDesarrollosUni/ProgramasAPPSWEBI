package mx.edu.utez.ExRecHectorSaldana.controller;

import mx.edu.utez.ExRecHectorSaldana.entity.Customer;
import mx.edu.utez.ExRecHectorSaldana.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/cliente")
    public List<Customer> getAll() {
        return customerService.getAll();
    }

    @GetMapping("/cliente/{id}")
    public ResponseEntity<Customer> getOne(@PathVariable("id") long id) {
        return customerService.getOne(id)
                .map(customer -> new ResponseEntity<>(customer,HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/cliente")
    public Customer insertOne(@RequestBody Customer customer) {
        return customerService.insertOne(customer);
    }

    @PutMapping("/cliente")
    public Customer updateOne(@RequestBody Customer customer) {
        return customerService.updateOne(customer);
    }

    @DeleteMapping("/cliente/{id}")
    public boolean deleteOne(@PathVariable("id") long id) {
        return customerService.deleteOne(id);
    }
}
