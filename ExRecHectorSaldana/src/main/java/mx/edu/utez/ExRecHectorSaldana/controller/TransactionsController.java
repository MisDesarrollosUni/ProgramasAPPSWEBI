package mx.edu.utez.ExRecHectorSaldana.controller;

import mx.edu.utez.ExRecHectorSaldana.entity.Transactions;
import mx.edu.utez.ExRecHectorSaldana.service.TransactionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class TransactionsController {

    @Autowired
    private TransactionsService transactionsService;

    @GetMapping("/movimiento")
    public List<Transactions> getAll(){
        return transactionsService.getAll();
    }

    @GetMapping("/movimiento/{id}")
    public ResponseEntity<Transactions> getOne(@PathVariable("id")long id){
        return transactionsService.getOne(id)
                .map(transactions -> new ResponseEntity<>(transactions, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/movimiento/{id}/{inicio}/{fin}")
    public List<Transactions> datesBetween(@PathVariable("id") long id,
                                           @PathVariable("inicio")String inicio,
                                           @PathVariable("fin")String fin) {
        return transactionsService.datesBetween(inicio, fin, id);
    }

    @PostMapping("/movimiento")
    public Transactions insertOne(@RequestBody Transactions transactions){
        return transactionsService.insertOne(transactions);
    }

    @PutMapping("/movimiento")
    public Transactions updateOne(@RequestBody Transactions transactions){
        return transactionsService.updateOne(transactions);
    }

    @DeleteMapping("/movimiento/{id}")
    public boolean deleteOne(@PathVariable("id")long id){
        return transactionsService.deleteOne(id);
    }

}
