package mx.edu.utez.ExRecHectorSaldana.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "tipo_cuenta")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountType implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "tipo")
    private String type;

    @Column(name = "estado")
    private boolean status;

    @OneToMany(mappedBy = "accountType")
    @JsonIgnore
    private List<CustomerAccount> customerAccounts;

}
