package mx.edu.utez.ExRecHectorSaldana.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "cliente")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "nombre")
    private String name;

    @Column(name = "primerApellido")
    private String firstSurname;

    @Column(name = "segundoApellido")
    private String secondSurname;

    @Column(name = "correo_electronico")
    private String email;

    @Column(name = "estado")
    private boolean status;

    @OneToMany(mappedBy = "customer")
    @JsonIgnore
    private List<CustomerAccount> customerAccounts;

}
