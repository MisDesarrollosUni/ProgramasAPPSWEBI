package mx.edu.utez.ExRecHectorSaldana.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "cuenta_cliente")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CustomerAccount implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "monto_maximo")
    private double maxAmount;

    @Column(name = "monto_actual")
    private double currentAmount;

    @Column(name = "estado")
    private boolean status;

    @ManyToOne
    @JoinColumn(name = "cliente_id")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "tipo_cuenta_id")
    private AccountType accountType;

    @OneToMany(mappedBy = "customerAccount")
    @JsonIgnore
    private List<Transactions> transactions;

}
