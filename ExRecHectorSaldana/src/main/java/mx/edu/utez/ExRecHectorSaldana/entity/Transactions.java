package mx.edu.utez.ExRecHectorSaldana.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "movimientos")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Transactions  implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "fecha_movimiento")
    private LocalDateTime dateTime;

    @Column(name = "operacion")
    private int operation;

    @Column(name = "monto")
    private double amount;

    @Column(name = "concepto")
    private String concept;

    @ManyToOne
    @JoinColumn(name = "cuenta_cliente_id")
    private CustomerAccount customerAccount;

}
