package mx.edu.utez.ExRecHectorSaldana.repository;

import mx.edu.utez.ExRecHectorSaldana.entity.AccountType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountTypeRepository extends JpaRepository<AccountType, Long> {
}
