package mx.edu.utez.ExRecHectorSaldana.repository;

import mx.edu.utez.ExRecHectorSaldana.entity.CustomerAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerAccountRepository extends JpaRepository<CustomerAccount, Long> {
}
