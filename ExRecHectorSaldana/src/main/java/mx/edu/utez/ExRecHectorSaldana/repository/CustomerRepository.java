package mx.edu.utez.ExRecHectorSaldana.repository;

import mx.edu.utez.ExRecHectorSaldana.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    @Query(value = "SELECT * from cliente where correo_electronico = :correo",nativeQuery = true)
    Customer searchByEmail(@Param("correo") String email);

}
