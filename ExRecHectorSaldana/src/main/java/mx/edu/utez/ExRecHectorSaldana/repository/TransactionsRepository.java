package mx.edu.utez.ExRecHectorSaldana.repository;

import mx.edu.utez.ExRecHectorSaldana.entity.Transactions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionsRepository extends JpaRepository<Transactions, Long> {
    @Query(value = "SELECT * FROM movimientos WHERE cuenta_cliente_id = :idTran and fecha_movimiento >= :inicio and fecha_movimiento  <= :fin",
            nativeQuery = true)
    List<Transactions> searchBetweenDatesAndId(@Param("idTran") long id,@Param("inicio") String inicio,@Param("fin") String fin);
}
