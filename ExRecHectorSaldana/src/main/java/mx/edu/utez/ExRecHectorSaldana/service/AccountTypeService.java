package mx.edu.utez.ExRecHectorSaldana.service;

import mx.edu.utez.ExRecHectorSaldana.entity.AccountType;
import mx.edu.utez.ExRecHectorSaldana.repository.AccountTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AccountTypeService {

    @Autowired
    private AccountTypeRepository accountTypeRepository;

    public List<AccountType> getAll() {
        return accountTypeRepository.findAll();
    }

    public Optional<AccountType> getOne(long id) {
        return accountTypeRepository.findById(id);
    }

    public AccountType insertOne(AccountType accountType) {
        return accountTypeRepository.save(accountType);
    }

    public AccountType updateOne(AccountType accountType) {
        if (getOne(accountType.getId()) != null) {
            return accountTypeRepository.save(accountType);
        }
        return null;
    }

    public boolean deleteOne(long id) {
        if (getOne(id) != null) {
            accountTypeRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
