package mx.edu.utez.ExRecHectorSaldana.service;

import mx.edu.utez.ExRecHectorSaldana.entity.AccountType;
import mx.edu.utez.ExRecHectorSaldana.entity.Customer;
import mx.edu.utez.ExRecHectorSaldana.entity.CustomerAccount;
import mx.edu.utez.ExRecHectorSaldana.repository.AccountTypeRepository;
import mx.edu.utez.ExRecHectorSaldana.repository.CustomerAccountRepository;
import mx.edu.utez.ExRecHectorSaldana.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerAccountService {
    @Autowired
    private CustomerAccountRepository customerAccountRepository;
    @Autowired
    private AccountTypeService accountTypeService;
    @Autowired
    private CustomerService customerService;

    public List<CustomerAccount> getAll() {
        return customerAccountRepository.findAll();
    }

    public Optional<CustomerAccount> getOne(long id) {
        return customerAccountRepository.findById(id);
    }

    public CustomerAccount insertOne(CustomerAccount customerAccount) {
        AccountType accountType = accountTypeService.getOne(customerAccount.getAccountType().getId()).get();
        Customer customer = customerService.getOne(customerAccount.getCustomer().getId()).get();
        String type = accountType.getType().toUpperCase();
        double amount = 0;
        switch (type){
            case "DEBITO":
            case "DÉBITO":
                amount = 5;
                break;
            case "CREDITO":
            case "CRÉDITO":
                amount = 7000;
                break;
            case "DEPARTAMENTAL":
                amount = 1500;
                break;
        }
        customerAccount.setAccountType(accountType);
        customerAccount.setCustomer(customer);
        customerAccount.setCurrentAmount(amount);
        customerAccount.setMaxAmount(amount);
        customerAccount.setStatus(true);
        return customerAccountRepository.save(customerAccount);
    }

    public CustomerAccount updateOne(CustomerAccount customerAccount) {
        if (getOne(customerAccount.getId()) != null) {
            return customerAccountRepository.save(customerAccount);
        } else {
            return null;
        }
    }

    public boolean deleteOne(long id) {
        if (getOne(id) != null) {
            customerAccountRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

}
