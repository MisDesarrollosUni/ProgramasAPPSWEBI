package mx.edu.utez.ExRecHectorSaldana.service;

import mx.edu.utez.ExRecHectorSaldana.entity.Customer;
import mx.edu.utez.ExRecHectorSaldana.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private EmailService emailService;

    public List<Customer> getAll() {
        return customerRepository.findAll();
    }

    public Optional<Customer> getOne(long id) {
        return customerRepository.findById(id);
    }

    public boolean isUseEmail(String email) {
        if (customerRepository.searchByEmail(email) != null) {
            return true;
        } else {
            return false;
        }
    }

    public Customer insertOne(Customer customer) {
        if (!isUseEmail(customer.getEmail())) {
            emailService.sendSimpleMail(customer.getEmail(), customer.getName());
            return customerRepository.save(customer);
        } else {
            return null;
        }
    }

    public Customer updateOne(Customer customer) {
        if (getOne(customer.getId()) != null && !isUseEmail(customer.getEmail())) {
            return customerRepository.save(customer);
        } else {
            return null;
        }
    }

    public boolean deleteOne(long id) {
        if (getOne(id) != null) {
            customerRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }


}
