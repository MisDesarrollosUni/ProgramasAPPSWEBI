package mx.edu.utez.ExRecHectorSaldana.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService {
    @Autowired
    private JavaMailSender mailSender;

    public void sendSimpleMail(String email, String name) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(email);
        message.setSubject("Bienvenido a nuestro banco " + name);
        message.setText(String.format("Hola %s, te damos una cordial bienvenida, y agradecemos que te hayas unido a nosotros.", email));
        mailSender.send(message);
    }
}
