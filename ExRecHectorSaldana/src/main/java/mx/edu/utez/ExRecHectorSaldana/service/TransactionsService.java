package mx.edu.utez.ExRecHectorSaldana.service;

import mx.edu.utez.ExRecHectorSaldana.entity.AccountType;
import mx.edu.utez.ExRecHectorSaldana.entity.CustomerAccount;
import mx.edu.utez.ExRecHectorSaldana.entity.Transactions;
import mx.edu.utez.ExRecHectorSaldana.repository.TransactionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TransactionsService {
    @Autowired
    private TransactionsRepository transactionsRepository;

    @Autowired
    private CustomerAccountService customerAccountService;

    @Autowired
    private AccountTypeService accountTypeService;

    public List<Transactions> getAll() {
        return transactionsRepository.findAll();
    }

    public Optional<Transactions> getOne(long id) {
        return transactionsRepository.findById(id);
    }

    public Transactions insertOne(Transactions transactions) {
        /*
         * Debito
         * 1. - Deposito +
         * 2. - Retiro        Concepto: Retiro cajeo -
         *
         * Credito
         * 1. - Compra -
         * 2. - Pago  +
         * 3. - Retiro       Concepto: Retiro cajeo   -
         *
         * Departamental
         * 1. Abono          Concepto: Pago de abono +
         * 2. Compra  -
         * */

        CustomerAccount customerAccount = customerAccountService.getOne(transactions.getCustomerAccount().getId()).get();
        AccountType accountType = accountTypeService.getOne(customerAccount.getAccountType().getId()).get();
        String type = accountType.getType().toUpperCase();
        double total = 0;
        double transacionAmount = transactions.getAmount();
        double customerCurrenAmount =  customerAccount.getCurrentAmount();
        transactions.setConcept("Hubo un movimiento");

        switch (type) {
            case "DEBITO":
            case "DÉBITO":
                switch (transactions.getOperation()) {
                    case 1:
                        total = transacionAmount + customerCurrenAmount;
                        break;
                    case 2:
                        total = transacionAmount - customerCurrenAmount;
                        transactions.setConcept("Retiro cajero");
                        break;
                }
                break;
            case "CREDITO":
            case "CRÉDITO":
                switch (transactions.getOperation()) {
                    case 1:
                        total = transacionAmount - customerCurrenAmount;
                        break;
                    case 2:
                        total = transacionAmount + customerCurrenAmount;
                        break;
                    case 3:
                        total = transacionAmount - customerCurrenAmount;
                        transactions.setConcept("Retiro cajero");
                        break;
                }
                break;
            case "DEPARTAMENTAL":
                switch (transactions.getOperation()) {
                    case 1:
                        total = transacionAmount + customerCurrenAmount;
                        transactions.setConcept("Pago de abono");
                        break;
                    case 2:
                        break;
                }
                break;
        }
        customerAccount.setCurrentAmount(total);
        transactions.setDateTime(LocalDateTime.now());
        customerAccountService.updateOne(customerAccount);
        System.out.println(customerAccount.getCurrentAmount());
        return transactionsRepository.save(transactions);
    }

    public Transactions updateOne(Transactions transactions) {
        if (getOne(transactions.getId()) != null) {
            return transactionsRepository.save(transactions);
        } else {
            return null;
        }
    }

    public boolean deleteOne(long id) {
        if (getOne(id) != null) {
            return true;
        } else {
            return false;
        }
    }

    public List<Transactions> datesBetween(String startDate, String endDate, long id) {
        /*DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime start = LocalDateTime.parse(startDate,formatter);
        LocalDateTime end = LocalDateTime.parse(endDate,formatter);*/
        return transactionsRepository.searchBetweenDatesAndId(id,startDate, endDate);
    }
}
