package mx.edu.utez.controller;

import javax.validation.Valid;
import mx.edu.utez.service.PersonService;
import mx.edu.utez.service.SquareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import mx.edu.utez.entity.Person;

@Controller
@RequestMapping("/persona")
public class PersonController {

	@Autowired
	private PersonService personService;
	@Autowired
	private SquareService squareService;

	@GetMapping("/principal")
	public String list(Model model) {
		model.addAttribute("personList", personService.getAll());
		return "index";
	}

	@GetMapping("/nuevo")
	public String create(Person person, Model model) {
		model.addAttribute("squareList", squareService.getAllStatusIsTrue());
		return "vistas/personanueva";
	}

	@PostMapping("/save")
	public String save(@Valid Person person, BindingResult result, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("squareList", squareService.getAllStatusIsTrue());
			return "vistas/personanueva";
		} else {
			if (person.getAge() >= 18 && person.getAge() < 23) {
				person.setStatus("Pendiente");
			} else {
				person.setStatus("Aprobado");
			}
			personService.saveOrUpdate(person);
			return "redirect:/persona/principal";
		}
	}

	@GetMapping("/editar/{id}")
	public String edit(@PathVariable("id") long id, Model model) {
		model.addAttribute("squareList", squareService.getAllStatusIsTrue());
		model.addAttribute("person", personService.getOne(id));
		return "vistas/personamodificar";
	}

	@PostMapping("/update/{id}")
	public String update(@PathVariable("id") long id, @Valid Person person, BindingResult result) {
		if (result.hasErrors()) {
			person.setId(id);
			return "vistas/personamodificar";
		} else {
			if (person.getAge() >= 18 && person.getAge() < 23) {
				person.setStatus("Pendiente");
			} else {
				person.setStatus("Aprobado");
			}
			personService.saveOrUpdate(person);
			return "redirect:/persona/principal";
		}
	}

	@GetMapping("/eliminar/{id}")
	public String delete(@PathVariable("id") long id) {
		personService.remove(id);
		return "redirect:/persona/principal";
	}

}
