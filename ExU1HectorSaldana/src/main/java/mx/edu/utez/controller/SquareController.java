package mx.edu.utez.controller;

import mx.edu.utez.service.SquareService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import mx.edu.utez.entity.Square;
import org.springframework.validation.BindingResult;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@RequestMapping("/casilla")
public class SquareController {

	@Autowired
	private SquareService squareService;

	@GetMapping("/principal")
	public String list(Model model) {
		model.addAttribute("squareList", squareService.getAll());
		return "vistas/casillas";
	}

	@GetMapping("/nuevo")
	public String create(Square square) {
		return "vistas/casillanueva";
	}

	@PostMapping("/save")
	public String save(@Valid Square square, BindingResult result) {
		if (result.hasErrors()) {
			return "vistas/casillanueva";
		} else {
			square.setStatus(true);
			squareService.saveOrUpdate(square);
			return "redirect:/casilla/principal";
		}
	}
	
	@GetMapping("/editar/{id}")
	public String edit(@PathVariable("id") long id, Model model) {
		model.addAttribute("square", squareService.getOne(id));
		return "vistas/casillamodificar";
	}
	
	@PostMapping("/update/{id}")
	public String update(@PathVariable("id") long id, @Valid Square square, BindingResult result) {
		if(result.hasErrors()) {
			square.setId(id);
			return "vistas/casillamodificar";
		}else {
			squareService.saveOrUpdate(square);
			return "redirect:/casilla/principal";
		}
	}

	@GetMapping("eliminar/{id}")
	public String delete(@PathVariable("id") long id){
		squareService.remove(id);
		return "redirect:/casilla/principal";
	}
	
}
