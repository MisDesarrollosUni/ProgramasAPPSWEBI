package mx.edu.utez.entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@Table(name="persona")
public class Person {

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "nombre", nullable = false)
	@NotBlank(message = "El nombre no debe estár vacío")
	private String name;
	
	@Column(name = "telefono", nullable = false)
	@NotBlank(message = "El teléfono no debe estár vacío")
	@Length(min = 10, max = 10, message = "Debe tener 10 dígitos")
	private String phone;
	
	@Column(name = "correo_electronico", nullable = false)
	@NotBlank(message = "El correo no debe estár vacío")
	@Pattern(regexp = "([a-z0-9]+(\\.?[a-z0-9])*)+@(([a-z]+)\\.([a-z]+))+", message="El correo debe ser válido")
	private String email;
	
	@Column(name = "edad", nullable = false)
	@Min(value = 18, message = "La edad debe ser mayor o igual a 18 años")
	private int age;
	
	@Column(name = "estado", nullable = false)
	private String status;

	@ManyToOne
	@NotNull(message = "No debe estár vacía")
	@JoinColumn(name = "casilla_id")
	private Square square;

	public Person(long id, @NotBlank(message = "El nombre no debe estár vacío") String name, @NotBlank(message = "El teléfono no debe estár vacío") String phone, @NotBlank(message = "El correo no debe estár vacío") @Pattern(regexp = "([a-z0-9]+(\\.?[a-z0-9])*)+@(([a-z]+)\\.([a-z]+))+", message = "El correo debe ser válido") String email, @NotBlank(message = "La edad no debe estár vacía") @Min(value = 18, message = "La edad debe ser mayor a 18 años") int age, String status, Square square) {
		this.id = id;
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.age = age;
		this.status = status;
		this.square = square;
	}

	public Person() {

	}

	public Square getSquare() {
		return square;
	}

	public void setSquare(Square square) {
		this.square = square;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", name=" + name + ", phone=" + phone + ", email=" + email + ", square=" + square
				+ ", age=" + age + ", status=" + status + "]";
	}
}
