package mx.edu.utez.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
@Table(name="casilla")
public class Square {

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "nombre", nullable = false)
	@NotBlank(message = "El nombre no debe estár vacío")
	private String name;
	
	@Column(name = "estado", nullable = false)
	private boolean status;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "square")
	private List<Person> persons;


	public Square(long id, @NotBlank(message = "El nombre no debe estár vacío") String name, boolean status, List<Person> persons) {
		this.id = id;
		this.name = name;
		this.status = status;
		this.persons = persons;
	}

	public Square() {
	
	}

	public List<Person> getPersons() {
		return persons;
	}

	public void setPersons(List<Person> persons) {
		this.persons = persons;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String nombre) {
		this.name = nombre;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Square [id=" + id + ", name=" + name + ", status=" + status + "]";
	}

	
	
	
}
