package mx.edu.utez.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import mx.edu.utez.entity.Person;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long>{

}
