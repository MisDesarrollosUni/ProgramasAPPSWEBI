package mx.edu.utez.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import mx.edu.utez.entity.Square;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SquareRepository extends JpaRepository<Square, Long>{
    List<Square> findAllByStatusIsTrue();
    List<Square> findByStatusIsTrue();
}
