package mx.edu.utez.service;

import mx.edu.utez.entity.Person;
import mx.edu.utez.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public List<Person> getAll(){
        return personRepository.findAll();
    }

    public Person getOne(long id){
        return personRepository.findById(id).get();
    }

    public Person saveOrUpdate(Person person){
        return personRepository.save(person);
    }

    public void remove(long id){
        personRepository.deleteById(id);
    }
}
