package mx.edu.utez.service;

import mx.edu.utez.entity.Square;
import mx.edu.utez.repository.SquareRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SquareService {

    @Autowired
    private SquareRepository squareRepository;

    public List<Square> getAll(){
        return squareRepository.findAll();
    }

    public Square getOne(long id){
        return squareRepository.findById(id).get();
    }

    public Square saveOrUpdate(Square square){
        return squareRepository.save(square);
    }

    public void  remove (long id){
        squareRepository.deleteById(id);
    }

    public List<Square> getAllStatusIsTrue(){
        return squareRepository.findAllByStatusIsTrue();
    }
}
