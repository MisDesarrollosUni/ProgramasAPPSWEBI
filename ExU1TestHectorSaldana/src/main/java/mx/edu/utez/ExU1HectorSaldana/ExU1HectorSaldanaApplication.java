package mx.edu.utez.ExU1HectorSaldana;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExU1HectorSaldanaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExU1HectorSaldanaApplication.class, args);
	}

}
