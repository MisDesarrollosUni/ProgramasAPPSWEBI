package mx.edu.utez.ExU1HectorSaldana.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.ui.Model;
import mx.edu.utez.ExU1HectorSaldana.entity.Consultation;
import mx.edu.utez.ExU1HectorSaldana.repository.ConsultationRepository;

@Controller
@RequestMapping("/veterinaria")
public class ConsultationController {
	
	@Autowired
	private ConsultationRepository consultationRepository;
	
	@GetMapping("/principal")
	public String list(Model model) {
		model.addAttribute("consultationsList", consultationRepository.findAll());
		return "index";
	}
	
	@GetMapping("/nuevo")
	public String create(Consultation consultation) {
		return "views/nueva";
	}
	
	@PostMapping("/save")
	public String save(@Valid Consultation consultation, BindingResult result) {
		if(result.hasErrors()) {
			return "views/nueva";
		}	
		else {
			consultationRepository.save(consultation);
			return "redirect:/veterinaria/principal";
		}	
	}
	
	@GetMapping("/editar/{id}")
	public String edit(@PathVariable("id") long id, Model model) {
		model.addAttribute("consultation", consultationRepository.findById(id).get());
		return "views/modificar";
	}
	
	@PostMapping("/update/{id}")
	public String update(@PathVariable("id") long id, @Valid Consultation consultation, BindingResult result) {
		if(result.hasErrors()) {
			consultation.setId(id);
			return "views/modificar";
		}else {
			consultationRepository.save(consultation);
			return "redirect:/veterinaria/principal";
		}
	}
	
	@GetMapping("/eliminar/{id}")
	public String delete(@PathVariable("id") long id) {
		Consultation consultation = consultationRepository.findById(id).get();
		consultationRepository.delete(consultation);
		return "redirect:/veterinaria/principal";
	}		
}
