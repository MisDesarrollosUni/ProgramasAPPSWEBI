package mx.edu.utez.ExU1HectorSaldana.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "consulta")
public class Consultation {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "nombre_paciente", nullable = false)
	@NotBlank(message = "El nombre no debe estár vacío")
	@Size(min=3, max = 30, message="Nombre entre 3 y 30 caracteres")
	private String name;
	
	@Column(name="numero_telefono", nullable=false)
	@NotBlank(message="El teléfono no debe estar vacío")
	@Size(min=10, max = 10, message="Teléfono de 10 caracteres")
	private String phone;

	@Column(name = "correo_electronico", nullable = false)
	@NotBlank(message = "El correo no debe estár vacío")
	@Size(min=5, max = 30, message="Correo entre 5 y 30 caracteres")
	@Pattern(regexp = "([a-z0-9]+(\\.?[a-z0-9])*)+@(([a-z]+)\\.([a-z]+))+", message="El correo debe ser válido")
	private String email;

	@Column(name = "motivo_visita", nullable = false)
	@NotBlank(message = "El motivo de la visita no debe estár vacío")
	@Size(min=3, max = 50, message="Motivo entre 5 y 30 caracteres")
	private String motive;

	@Column(name = "medicamento_recetado", nullable = false)
	@NotBlank(message = "El medicamento recetado no debe estár vacío")
	@Size(min=5, max = 50, message="Medicamento entre 5 y 30 caracteres")
	private String medicine;

	public Consultation(String name, String phone, String email, String motive, String medicine) {
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.motive = motive;
		this.medicine = medicine;
	}

	public Consultation() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMotive() {
		return motive;
	}

	public void setMotive(String motive) {
		this.motive = motive;
	}

	public String getMedicine() {
		return medicine;
	}

	public void setMedicine(String medicine) {
		this.medicine = medicine;
	}

	@Override
	public String toString() {
		return "Consultation [id=" + id + ", name=" + name + ", email=" + email + ", motive=" + motive + ", medicine="
				+ medicine + "]";
	}

}
