package mx.edu.utez.ExU1HectorSaldana.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.edu.utez.ExU1HectorSaldana.entity.Consultation;

public interface ConsultationRepository extends JpaRepository<Consultation, Long> {
	
}
