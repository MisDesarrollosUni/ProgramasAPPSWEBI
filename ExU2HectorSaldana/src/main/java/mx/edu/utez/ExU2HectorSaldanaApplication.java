package mx.edu.utez;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExU2HectorSaldanaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExU2HectorSaldanaApplication.class, args);
	}

}
