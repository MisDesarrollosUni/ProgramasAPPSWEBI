package mx.edu.utez.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.edu.utez.entity.AccountCustomer;
import mx.edu.utez.service.AccountCustomerService;

@RestController
@RequestMapping("/api")
public class AccountCustomerController {
	
	@Autowired
	private AccountCustomerService accountCustomerService;
	
	@GetMapping("/cuenta_liente")
	public List<AccountCustomer>  list(){
		return accountCustomerService.list();
	}
	
	// @PathVariable
	@PostMapping("/cuenta_liente")
	public AccountCustomer save(@RequestBody AccountCustomer accountCustomer ) {
		return accountCustomerService.save(accountCustomer);
	}
	
	@DeleteMapping("/cuenta_liente/{id}")
	public void delete(@PathVariable("id") long id) {
		accountCustomerService.delete(id);
	}
		
}
