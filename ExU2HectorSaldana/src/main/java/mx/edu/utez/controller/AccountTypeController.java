package mx.edu.utez.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.edu.utez.entity.AccountType;
import mx.edu.utez.service.AccountTypeService;

@RestController
@RequestMapping("/api")
public class AccountTypeController {
	@Autowired
	private AccountTypeService accountTypeService;
	
	@GetMapping("/tipo_cuenta")
	public List<AccountType> list(){
		return accountTypeService.list();
	}
	
	@PostMapping("/tipo_cuenta")
	public AccountType save(@RequestBody AccountType accountType) {
		return accountTypeService.save(accountType);
	}
	
	@DeleteMapping("/tipo_cuenta/{id}")
	public void delete(@PathVariable("id") long id) {
		accountTypeService.delete(id);
	}
	
}
