package mx.edu.utez.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.edu.utez.entity.Customer;
import mx.edu.utez.service.CustomerService;

@RestController
@RequestMapping("/api")
public class CustomerController {

	@Autowired
	private CustomerService customerService;
	
	@GetMapping("/cliente")
	public List<Customer> list(){
		return customerService.list();
	}
	
	@PostMapping("/cliente")
	public Customer save(@RequestBody Customer customer) {
		return customerService.save(customer);
	}
	
	@DeleteMapping("/cliente/{id}")
	public void delete(@PathVariable("id") long id) {
		customerService.delete(id);
	}
	
}
