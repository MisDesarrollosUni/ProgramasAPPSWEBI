package mx.edu.utez.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.edu.utez.entity.Transactions;
import mx.edu.utez.service.TransactionsService;

@RestController
@RequestMapping("/api")
public class TransactionsController {

	@Autowired
	private TransactionsService transactionsService;
	
	@GetMapping("/transacciones")
	public List<Transactions> list(){
		return transactionsService.list();
	}
	
	@PostMapping("/transacciones")
	public Transactions save(@RequestBody Transactions transactions ) {
		return transactionsService.save(transactions);
	}
	
	@DeleteMapping("/transacciones/{id}")
	public void delete(@PathVariable("id") long id) {
		transactionsService.delete(id);
	}
}
