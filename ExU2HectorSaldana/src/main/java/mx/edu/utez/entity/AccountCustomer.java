package mx.edu.utez.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "Cuenta_cliente")
public class AccountCustomer {

		
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private double maxAmount;
	
	private double currentlyMount;
	
	private boolean state;
	
	@ManyToOne
	@JoinColumn(name="customer_id", insertable = false, updatable = false)
	private Customer costumer;
	
	@ManyToOne
	@JoinColumn(name="accountTpey_id", insertable = false, updatable = false)
	private AccountType accountType;
	
	@OneToMany(mappedBy = "accountCustomer")
	private List<Transactions> list;




	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(double maxAmount) {
		this.maxAmount = maxAmount;
	}

	public double getCurrentlyMount() {
		return currentlyMount;
	}

	public void setCurrentlyMount(double currentlyMount) {
		this.currentlyMount = currentlyMount;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public Customer getCostumer() {
		return costumer;
	}

	public void setCostumer(Customer costumer) {
		this.costumer = costumer;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public List<Transactions> getList() {
		return list;
	}

	public void setList(List<Transactions> list) {
		this.list = list;
	}
	
	
	
	
	
}
