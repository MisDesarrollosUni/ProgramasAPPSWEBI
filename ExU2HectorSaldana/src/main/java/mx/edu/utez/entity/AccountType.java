package mx.edu.utez.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Tipo_cuenta")
public class AccountType {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String type;
	
	private boolean state;
	
	@OneToMany(mappedBy = "accountType")
	private List<AccountCustomer> acountCustmer;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public List<AccountCustomer> getAcountCustmer() {
		return acountCustmer;
	}

	public void setAcountCustmer(List<AccountCustomer> acountCustmer) {
		this.acountCustmer = acountCustmer;
	}
	
	
	
}
