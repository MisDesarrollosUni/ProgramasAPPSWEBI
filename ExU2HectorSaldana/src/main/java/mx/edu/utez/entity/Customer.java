package mx.edu.utez.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Cliente")
public class Customer {
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	
	private String name;
	
	private String firstApe;
	
	private String secondApe;
	
	private String email;
	
	private boolean state;
	
	
	@OneToMany(mappedBy = "costumer")
	private List<AccountCustomer> acountCustmer;




	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getFirstApe() {
		return firstApe;
	}


	public void setFirstApe(String firstApe) {
		this.firstApe = firstApe;
	}


	public String getSecondApe() {
		return secondApe;
	}


	public void setSecondApe(String secondApe) {
		this.secondApe = secondApe;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public boolean isState() {
		return state;
	}


	public void setState(boolean state) {
		this.state = state;
	}


	public List<AccountCustomer> getAcountCustmer() {
		return acountCustmer;
	}


	public void setAcountCustmer(List<AccountCustomer> acountCustmer) {
		this.acountCustmer = acountCustmer;
	}
	
	
	
}
