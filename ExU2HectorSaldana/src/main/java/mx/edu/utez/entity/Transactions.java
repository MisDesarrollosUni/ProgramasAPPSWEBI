package mx.edu.utez.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Transacciones")
public class Transactions {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	private long id;
	
	private String dateTransaction;
	
	private int operation;
	
	private double amount;
	
	private String conept;
	
	@ManyToOne
	@JoinColumn(name="accountCostumer_id", insertable = false, updatable = false)
	private AccountCustomer accountCustomer;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDateTransaction() {
		return dateTransaction;
	}

	public void setDateTransaction(String dateTransaction) {
		this.dateTransaction = dateTransaction;
	}

	public int getOperation() {
		return operation;
	}

	public void setOperation(int operation) {
		this.operation = operation;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getConept() {
		return conept;
	}

	public void setConept(String conept) {
		this.conept = conept;
	}

	public AccountCustomer getAccountCustomer() {
		return accountCustomer;
	}

	public void setAccountCustomer(AccountCustomer accountCustomer) {
		this.accountCustomer = accountCustomer;
	}
	
	
	
}
