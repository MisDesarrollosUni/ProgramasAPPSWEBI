package mx.edu.utez.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.edu.utez.entity.AccountCustomer;

@Repository
public interface AccountCustomerRepository extends JpaRepository<AccountCustomer, Long> {

}
