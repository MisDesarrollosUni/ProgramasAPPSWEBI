package mx.edu.utez.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.edu.utez.entity.AccountType;

@Repository
public interface AccountTypeRepository extends JpaRepository<AccountType, Long>{

}
