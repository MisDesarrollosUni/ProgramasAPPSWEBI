package mx.edu.utez.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.edu.utez.entity.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>{

}
