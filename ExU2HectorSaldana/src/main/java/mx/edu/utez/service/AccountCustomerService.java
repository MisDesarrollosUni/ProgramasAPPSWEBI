package mx.edu.utez.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import mx.edu.utez.entity.AccountCustomer;
import mx.edu.utez.entity.AccountType;
import mx.edu.utez.repository.AccountCustomerRepository;
import mx.edu.utez.repository.AccountTypeRepository;

@Service
public class AccountCustomerService {
	
	@Autowired
	private AccountCustomerRepository accountCustomerRepository;
	@Autowired
	private AccountTypeRepository accountTypeRepository;

	public List<AccountCustomer> list(){
		return accountCustomerRepository.findAll();
	}
	
	public AccountCustomer getOne(long id) {
		return accountCustomerRepository.findById(id).get();
	}
	
	public AccountCustomer save(AccountCustomer accountCustomer) {
		AccountType tipoCuenta = accountTypeRepository.getOne(accountCustomer.getAccountType().getId());
		switch(tipoCuenta.getType()) {
			case "Débito":{
				accountCustomer.setCurrentlyMount(5);
				accountCustomer.setMaxAmount(5);
				break;
			}
			case "Crédito":{
				accountCustomer.setCurrentlyMount(7000);
				accountCustomer.setMaxAmount(7000);
				break;
			}
			case "Departamental":{
				accountCustomer.setCurrentlyMount(1500);
				accountCustomer.setMaxAmount(1500);
				break;
			}
		}
		
		return accountCustomerRepository.save(accountCustomer);
	}	

	public void update(AccountCustomer cuenta) {
		accountCustomerRepository.save(cuenta);
	}
	
	public void delete(long id) {
		accountCustomerRepository.deleteById(id);
	}

}
