package mx.edu.utez.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import mx.edu.utez.entity.AccountType;
import mx.edu.utez.repository.AccountTypeRepository;

@Service
public class AccountTypeService {

	@Autowired
	private AccountTypeRepository accountTypeRepository;
	
	public List<AccountType> list(){
		return accountTypeRepository.findAll();
	}
	
	public AccountType getOne(long id) {
		return accountTypeRepository.findById(id).get();
	}
	
	public AccountType save(AccountType accountType) {
		return accountTypeRepository.save(accountType);
	}	
	
	public void delete(long id) {
		accountTypeRepository.deleteById(id);
	}
	
}
