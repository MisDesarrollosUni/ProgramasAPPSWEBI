package mx.edu.utez.service;

import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import mx.edu.utez.entity.Customer;
import mx.edu.utez.repository.CustomerRepository;

@Service
public class CustomerService {

	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private EmailService emailService;
	
	public List<Customer> list(){
		return customerRepository.findAll();
	}
	
	public Customer getOne(long id) {
		return customerRepository.findById(id).get();
	}
	
	public Customer save(Customer customer) {
		try {
			
			List<Customer> listCostumer = list();
			for(Customer c: listCostumer) {
				if(c.getEmail().equalsIgnoreCase(customer.getEmail())) {
					return null;
				}
			}
			
			emailService.sendTemplateMail(customer.getEmail());
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return customerRepository.save(customer);
	}	
	
	public void delete(long id) {
		customerRepository.deleteById(id);
	}
	
}
