package mx.edu.utez.service;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Service
public class EmailService {

	@Autowired
	private JavaMailSender mailSender;
	
	@Autowired
	private SpringTemplateEngine templateEngine;
	
	public void sendSimpleMail(String email) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(email);
		message.setSubject("Mi primer correo con Spring boot");
		message.setText(String.format("Hola %s, bienvenido a mi página", email));
		mailSender.send(message);
	}
	
	public void sendTemplateMail(String email) throws MessagingException {
		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper help = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_RELATED,StandardCharsets.UTF_8.name());
		
		Map<String, Object>  map = new 	HashMap<String, Object>(); // Creamos un mapa
		map.put("correo", email); // agregamos todos los valores que quedramos importar
		Context context = new Context(); // creamos un context para enviar los datos
		context.setVariables(map); // y lo agregamos es como si fuera model.addAtributte()
		
								// el archivo se llama micorreo sin .html
		String html = templateEngine.process("micorreo", context); // renderizamos 
		
		help.setTo(email); // Colocamos el email
		help.setText(html, true); // Colocamos el cuerpo
		help.setSubject("Mi correo con formato bonito"); // Colocamos un asunto
		mailSender.send(message); // Enviamos el email
		
	}
}
