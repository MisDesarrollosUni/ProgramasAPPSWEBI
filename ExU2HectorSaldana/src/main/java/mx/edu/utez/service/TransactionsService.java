package mx.edu.utez.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.edu.utez.entity.AccountCustomer;
import mx.edu.utez.entity.Transactions;
import mx.edu.utez.repository.TransactionsRepository;

@Service
public class TransactionsService {

	@Autowired
	private TransactionsRepository transactionsRepository;
	@Autowired
	private AccountCustomerService accountCustomerService;
	
	
	public List<Transactions> list(){
		return transactionsRepository.findAll();
	}
	
	public Transactions getOne(long id) {
		return transactionsRepository.findById(id).get();
	}
	
	
	/*
	 * Débito 
	 * 1. Deposito
	 * 2. Retiro
	 * 
	 * Crédito
	 * 1. Compra
	 * 2. Pago
	 * 3. Retiro
	 * 
	 * Departamental
	 * 1. Abono
	 * 2. Compra
	 */
	public Transactions save(Transactions transactions) {
		AccountCustomer cuenta = accountCustomerService.getOne(transactions.getAccountCustomer().getAccountType().getId()); 
		double operaciones = 0;
		transactions.setConept("Mi transacción");
		switch(cuenta.getAccountType().getType()) {
			case "Bébito":{
				switch(transactions.getOperation()) {
					case 1:{
						operaciones = transactions.getAmount() + cuenta.getCurrentlyMount();
						cuenta.setCurrentlyMount(operaciones);
						break;
					}
					case 2:{
						operaciones =  cuenta.getCurrentlyMount() - transactions.getAmount();
						cuenta.setCurrentlyMount(operaciones);		
						transactions.setConept("Retiro cajero");
						break;
					}
					
				}
				accountCustomerService.update(cuenta);
				break;
			}
			case "Crédito":{
				switch(transactions.getOperation()) {
				case 1:{
					operaciones =  cuenta.getCurrentlyMount() - transactions.getAmount();
					cuenta.setCurrentlyMount(operaciones);	
					break;					
				}
				case 2:{
					operaciones =  cuenta.getCurrentlyMount() + transactions.getAmount();
					cuenta.setCurrentlyMount(operaciones);	
					break;
				}
				case 3:{
					operaciones =  cuenta.getCurrentlyMount() - transactions.getAmount();
					cuenta.setCurrentlyMount(operaciones);	
					transactions.setConept("Retiro cajero");
					break;
				}
				
				}
				accountCustomerService.update(cuenta);
				break;
			}
			case "Departamental":{
				switch(transactions.getOperation()) {
				case 1:{
					operaciones =  cuenta.getCurrentlyMount() + transactions.getAmount();
					cuenta.setCurrentlyMount(operaciones);
					transactions.setConept("Pago de abono");
					break;
				}
				case 2:{
					operaciones =  cuenta.getCurrentlyMount() - transactions.getAmount();
					cuenta.setCurrentlyMount(operaciones);
					break;
				}						
			}
				accountCustomerService.update(cuenta);
				break;
		}
		
		}
		return transactionsRepository.save(transactions);
	}	
	
	public void delete(long id) {
		transactionsRepository.deleteById(id);
	}
	
}
