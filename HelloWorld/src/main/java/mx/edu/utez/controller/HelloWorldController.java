package mx.edu.utez.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

//Le decimos que sera controlador (como Servlet) y afecta a toda la clase
@Controller
@RequestMapping("/hello")
public class HelloWorldController {

	@GetMapping("/lista") //Afecta al método
	public String hola(Model model) {
		model.addAttribute("saludo", "Hola, cómo estás?");
		return "index"; // el return va ligado al archivo html
	}
	// localhost:8080/hello/lista
}
