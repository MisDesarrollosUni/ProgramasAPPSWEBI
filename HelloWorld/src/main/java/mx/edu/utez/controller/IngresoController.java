package mx.edu.utez.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import mx.edu.utez.entity.Earn;
import mx.edu.utez.repository.EarnRepository;

@Controller
@RequestMapping("/mis-ingresos")
public class IngresoController {
	
	//http://localhost:8080/mis-ingresos/
	
	
	@Autowired //Crea una instancia - EarnRepository earnRepository = new EarnRepository();
	private EarnRepository earnRepository;
	
	
	@GetMapping("/list")
	public String list(Model model) {
		model.addAttribute("earnList", earnRepository.findAll());
		return "ingreso/index";
	}
	
	@GetMapping("/create")
	public String create(Earn earn) {
		return "ingreso/create";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") long id, Model model) {
		model.addAttribute("earn",earnRepository.findById(id));
		return "ingreso/edit";
	}
	
	@PostMapping("/save")
	public String save(@Valid Earn earn, BindingResult result) {
		if(result.hasErrors()) {
			return "ingreso/create";
		}else {
			earnRepository.save(earn);
			return "redirect:/mis-ingresos/list";
		}
	}
	
	@PostMapping("/update/{id}")
	public String update(@PathVariable("id") long id, @Valid Earn earn, BindingResult result) {
		if(result.hasErrors()) {
			earn.setId(id); // para enviar ese mismo id en el formulario, y no haya error
			return "redirect:/mis-ingresos/edit";
		}else {
			earnRepository.save(earn);
			return "redirect:/mis-ingresos/list";
		}
		
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") long id) {
		System.out.println("ID desde URI: " +id);
		Earn earn = earnRepository.findById(id).get();
		earnRepository.delete(earn);
		// earnRepository.deleteById(earn);
		return "redirect:/mis-ingresos/list";
	}
	
	
	// Valid es para validar los campos de la Entity.
}
