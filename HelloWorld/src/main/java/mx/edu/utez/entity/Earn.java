package mx.edu.utez.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name="ingreso") // la entidad se llamara ingreso y no Earn
public class Earn {
	
	@Id // Indica que el atributo que esta debajo será la llave primaria PK
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name="descripcion", nullable = false) // Permite cambiar el atributo a nivel base de datos, y agrega constrains
	@NotBlank(message = "La descripción es requerida") // Manda un mensaje de error en el método create y update con Valid y BingindResult y lo muestra en span
	private String description;
		
	@Column(name="fecha")
	private String date;
	
	@Column(name="monto")
	private double amount;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "Earn [id=" + id + ", description=" + description + ", date=" + date + ", amount=" + amount + "]";
	}
}
