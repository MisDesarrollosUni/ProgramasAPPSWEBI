package mx.edu.utez.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.edu.utez.entity.Earn;

@Repository
public interface EarnRepository extends JpaRepository<Earn, Long> {
	// T - Hace referencia a la Entity (Earn)
	// ID - Tipo de dato del id de la entity (Long)
}
