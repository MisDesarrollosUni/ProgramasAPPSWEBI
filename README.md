# 5C | ProgramasAPPSWEBI
## Aplicaciones Web Para La Industria 4.0 | IntelliJ 2020.1 🐱‍👤 && STS ⚙️
> Saldaña Espinoza Hector - Desarrollo de Software
>

### Contenido 🚀
#### Unidad 1 😎
* [ExRecHectorSaldana](https://github.com/HectorSaldes/ProgramasAPPSWEBI/tree/master/ExRecHectorSaldana) - Examen Test Recuperación
* [ExU1HectorSaldana](https://github.com/HectorSaldes/ProgramasAPPSWEBI/tree/master/ExU1HectorSaldana) - Examen Unidad Uno
* [ExU1TestHectorSaldana](https://github.com/HectorSaldes/ProgramasAPPSWEBI/tree/master/ExU1TestHectorSaldana) - Examen Test Unidad Uno
* [ExU2HectorSaldana](https://github.com/HectorSaldes/ProgramasAPPSWEBI/tree/master/ExU2HectorSaldana) - Examen Unidad Dos
* [HelloWorld](https://github.com/HectorSaldes/ProgramasAPPSWEBI/tree/master/HelloWorld) - Iniciando con STS
* [SOSP](https://github.com/HectorSaldes/ProgramasAPPSWEBI/tree/master/SOSP) - Projecto SOSP API
