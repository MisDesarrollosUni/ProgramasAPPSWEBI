package mx.edu.utvt.config;
/*
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter{

	// SOBREESCRIBIR METODO DE SEGURIDAD
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		// agregar seguridad a un endpoint
		http.csrf().disable() // deshabilita un protocolo, 
		.authorizeRequests() // toda api debe ser autenticada
		.antMatchers(HttpMethod.POST, "/api/areas").hasRole("ADMIN") //toda petición de tipo POST que se haga api/areas, 
		.and()														//solamente puede usarse con el rol admin
		.formLogin().disable() //quita el formulario de la web que miramos
		.httpBasic(); // sera autenticación básica, usuario y contraseña
	}
	
	
	// SOBREESCRIBIR METODO DE SESIÓN
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()// simulación de autenticación
		.withUser("hector") // colorcar un usuario
		.password("{noop}admin") // ese {noop} sirve para encriptar la contraseña con el valor de admin
		.roles("ADMIN"); // y el rol será ADMIN
	}
	
}
*/