package mx.edu.utvt.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration // Seraá de configuración
@EnableSwagger2 // Y usaremos Swagger
public class SwaggerConfig {

	// TE DOCUMENTA TODA TU API PARA EL PÚBLICO
	
	@Bean // Este método nos permite levantar swagger
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()			// Esto apunta a nuestros controlladores
				.apis(RequestHandlerSelectors.basePackage("mx.edu.utvt.controller"))
				.build();
	}
	
}
