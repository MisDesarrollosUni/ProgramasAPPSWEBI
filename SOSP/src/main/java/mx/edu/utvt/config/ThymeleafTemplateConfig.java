package mx.edu.utvt.config;

import java.nio.charset.StandardCharsets;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.templatemode.TemplateMode;

@Configuration
public class ThymeleafTemplateConfig {
	
	
	// MVC
	@Bean // Establecer que usaremos una plantilla
	public SpringTemplateEngine springTemplateEngine() {
		SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		templateEngine.addTemplateResolver(htmlTemplateResolve());
		return templateEngine;
	}
	
	
	@Bean
	public SpringResourceTemplateResolver htmlTemplateResolve() {
		SpringResourceTemplateResolver emailTemplateResolver = new SpringResourceTemplateResolver();
		emailTemplateResolver.setPrefix("classpath:/templates/emails/"); // Donde buscar el template en la ubicacion del proyecto
		emailTemplateResolver.setSuffix(".html"); // Que tipo de archivos buscara dentro de la ruta
		emailTemplateResolver.setTemplateMode(TemplateMode.HTML); // Templates de HTML
		emailTemplateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name()); // Usando UTF-8 para caracteres especiales
		return emailTemplateResolver;
	}
	
}
