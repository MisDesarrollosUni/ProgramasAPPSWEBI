package mx.edu.utvt.controller;

import mx.edu.utvt.entity.Area;
import mx.edu.utvt.service.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class AreaController {

    /**
     * h1 Ahora inyectamos de nuestro servicio, este ya tiene las reglas de negocio y conexión a la DB
     */
    @Autowired
    private AreaService areaService;

    @GetMapping("/areas")
    public List<Area> list() {
        return areaService.getAll();
    }

    @GetMapping("/areas/{id}")
    public Area edit(@PathVariable("id") long id) {
        return areaService.getArea(id);
    }


    @GetMapping("/areas/{criterio}")
    public List<Area> listByDecription(@PathVariable("criterio") String criterio){
        return areaService.filterByDescription(criterio);
    }

    /**
     * h1 @RequestBody recupera todos los atributos del área
     */
    @PostMapping("/areas")
    public Area save(@RequestBody Area area) {
        return areaService.saveOrUpdate(area);
    }

    @PutMapping("/areas")
    public Area update(@RequestBody Area area) {
        return areaService.saveOrUpdate(area);
    }

    @DeleteMapping("/areas/{id}")
    public void delete(@PathVariable("id") long id) {
        areaService.remove(id);
    }
    
}
