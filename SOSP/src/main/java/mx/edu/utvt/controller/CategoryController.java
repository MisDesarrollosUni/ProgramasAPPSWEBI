package mx.edu.utvt.controller;

import mx.edu.utvt.entity.Category;
import mx.edu.utvt.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:8081")
public class CategoryController {

    // *Ahora inyectamos de nuestro servicio, este ya tiene las reglas de negocio y conexión a la DB
    @Autowired
    private CategoryService categoryService;

    @GetMapping("/categorias")
    public List<Category> list(){
        return categoryService.getAll();
    }

    @GetMapping("/categorias/{id}")
    public Category edit(@PathVariable("id") long id){
        return categoryService.getCategory(id);
    }
    
    @GetMapping("/categorias/trues")
    public List<Category> listOnlyTrue(){
    	return categoryService.findAllByStatusIsTrue();
    }
    
    @PostMapping("/categorias")
    public Category save(@RequestBody Category category){
        return categoryService.saveOrUpdate(category);
    }

    @PutMapping("/categorias")
    public Category update(@RequestBody Category category){
        return categoryService.saveOrUpdate(category);
    }
    
    @DeleteMapping("categorias/{id}")
    public void delete(@PathVariable("id") long id){
        categoryService.remove(id);
    }
}
