package mx.edu.utvt.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


import mx.edu.utvt.entity.Document;
import mx.edu.utvt.service.DocumentService;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:8081")
public class DocumentController {

    @Autowired
    private DocumentService documentService;

    @GetMapping("/documentos")
    public List<Document> list() {
        return documentService.getAll();
    }

    @GetMapping("/documentos/{id}")
    public Document edit(@PathVariable("id") long id) {
        return documentService.getOne(id);
    }
    
    @PostMapping("/documentos")
    public void save(
    		@RequestParam("title") String title,
    		@RequestParam("content") String content, 
    		@RequestParam("categoryId") long categoryId,
    		@RequestParam("fileType") long fileTypeId,
    		@RequestParam("file") MultipartFile file){
    	
    		Document document = new Document();
			document.setTitle(title);
			document.setContent(content);
			documentService.saveOrUpdate(document, categoryId, fileTypeId, file);
		
    	
        //return documentService.saveOrUpdate(document);
    }

    /*
     * @PutMapping("/documentos")
    public Document update(@RequestBody Document document){
        return documentService.saveOrUpdate(document);
    }
    */
    
    @DeleteMapping("/documentos/{id}")
    public void delete(@PathVariable("id") long id){
        documentService.remove(id);
        
    }
   
}
