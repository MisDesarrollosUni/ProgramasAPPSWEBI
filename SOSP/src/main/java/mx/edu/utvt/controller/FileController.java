package mx.edu.utvt.controller;

import mx.edu.utvt.entity.File;
import mx.edu.utvt.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class FileController {

    @Autowired
    private FileService fileService;

    @GetMapping("/archivos")
    public List<File> list() {
        return fileService.getAll();
    }

    @GetMapping("/archivos/{id}")
    public File edit(@PathVariable("id") long id) {
        return fileService.getFile(id);
    }

    /*
    @PostMapping("/archivos")
    public ResponseEntity<File> save(@RequestParam("archivo") MultipartFile file, @RequestParam("tipoArchivo") long id ) {
    	try {
			fileService.saveOrUpdate(file, id);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<File>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    	return new ResponseEntity<File>(HttpStatus.OK);
    }*/

    /*
    @PutMapping("/archivos")
    public File update(@RequestBody File file){
        return fileService.saveOrUpdate(file);
    }
*/
    @DeleteMapping("/archivos/{id}")
    public void delete(@PathVariable("id") long id){
        fileService.remove(id);
    }
}
