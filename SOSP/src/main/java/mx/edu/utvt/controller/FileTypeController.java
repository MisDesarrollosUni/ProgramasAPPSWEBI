package mx.edu.utvt.controller;

import mx.edu.utvt.entity.FileType;
import mx.edu.utvt.service.FileTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:8081")
public class FileTypeController {

    // *Ahora inyectamos de nuestro servicio, este ya tiene las reglas de negocio y conexión a la DB
    @Autowired
    private FileTypeService fileTypeService;

    @GetMapping("/tipo-archivos")
    public List<FileType> list(){
        return fileTypeService.getAll();
    }

    @GetMapping("/tipo-archivos/{id}")
    public FileType edit(@PathVariable("id") long id){
        return fileTypeService.getFileType(id);
    }
    
    @PostMapping("/tipo-archivos")
    public FileType save(@RequestBody FileType fileType) {
    	return fileTypeService.saveOrUpdate(fileType);
    }
    
    @PutMapping("/tipo-archivos")
    public FileType update(@RequestBody FileType fileType) {
        return fileTypeService.saveOrUpdate(fileType);
    }

    @DeleteMapping("/tipo-archivos/{id}")
    public void delete(@PathVariable("id") long id){
        fileTypeService.remove(id);
    }
}
