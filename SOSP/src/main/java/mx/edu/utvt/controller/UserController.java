package mx.edu.utvt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.edu.utvt.config.JwtTokenUtil;
import mx.edu.utvt.entity.User;
import mx.edu.utvt.service.UserService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class UserController {
	
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/users")
	public ResponseEntity<List<User>> list() {
		List<User> userList = userService.getAll();
		if(userList.size() == 0) {
			return new ResponseEntity<List<User>>(userList,HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<List<User>>(userList,HttpStatus.OK);	
	}
	
	@PostMapping("/users")
	public ResponseEntity<User> save(@RequestBody User user) {
		User saveUser = userService.save(user);
		if(saveUser == null) {
			return new ResponseEntity<User>(saveUser,HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<User>(saveUser,HttpStatus.OK);
	}
	
}
