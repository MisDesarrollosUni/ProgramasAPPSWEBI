package mx.edu.utvt.controller;

import mx.edu.utvt.entity.UserDocument;
import mx.edu.utvt.service.UserDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class UserDocumentController {

    @Autowired
    private UserDocumentService userDocumentService;

    @GetMapping("/documentosusuarios")
    public List<UserDocument> list(){
        return userDocumentService.getAll();
    }

    @GetMapping("/documentosusuarios/{id}")
    public UserDocument edit(@PathVariable("id") long id){
        return userDocumentService.getUserDocument(id);
    }

    @PostMapping("/documentosusuarios")
    public UserDocument save(@RequestBody UserDocument userDocument){
        return  userDocumentService.saveOrUpdate(userDocument);
    }

    @PutMapping("/documentosusuarios")
    public UserDocument update(@RequestBody UserDocument userDocument){
        return  userDocumentService.saveOrUpdate(userDocument);
    }

    @DeleteMapping("/documentosusuarios/{id}")
    public void delete(@PathVariable("id") long id){
        userDocumentService.remove(id);
    }
}
