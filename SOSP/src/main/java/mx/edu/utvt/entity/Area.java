package mx.edu.utvt.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "area")
public class Area implements Serializable {
	
    private static final long serialVersionUID = 1L;

    /**
     * * El peso lo hace más ligero
     * h1 @DATA viene de lombok y tiene los getters y setters por implicito, ya no es necesario colocarlos explicitamenta
     * h2 @NoArgsConstructor cons
     * h2 @Getter
     * h2 @Setter
     */
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "descipcion")
    private String description;

    @Column(name = "estado")
    private boolean status;

    @OneToMany(mappedBy = "area")
    private List<User> user;
    
	public Area(String description, boolean status, List<User> user) {
		this.description = description;
		this.status = status;
		this.user = user;
	}

	
	public Area() {
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public List<User> getUser() {
		return user;
	}

	public void setUser(List<User> user) {
		this.user = user;
	}
}
