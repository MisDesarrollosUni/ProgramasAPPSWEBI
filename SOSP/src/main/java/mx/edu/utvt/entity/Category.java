package mx.edu.utvt.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "categoria")
public class Category implements Serializable {

    /**
     * * El peso lo hace más ligero
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "tipo")
    private String type;

    @Column(name = "estado")
    private boolean status;

    /**
     * * Una categoría tiene muchos documentos
     * h3 @mappedBy, tiene que coindicir con el atributo de Category en Document.class (Bean)
     * h3 y hacer match
     */
    
    @OneToMany(mappedBy = "category")
    private List<Document> document;
    
	public Category(String type, boolean status, List<Document> document) {
		this.type = type;
		this.status = status;
		this.document = document;
	}
	
	public Category() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public List<Document> getDocument() {
		return document;
	}

	public void setDocument(List<Document> document) {
		this.document = document;
	}
    
    

}
