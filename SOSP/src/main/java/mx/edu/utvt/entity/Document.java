package mx.edu.utvt.entity;


import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "documento")
@Builder
//@NoArgsConstructor
@AllArgsConstructor
public class Document implements Serializable {
    /**
     * h1 El peso lo hace más ligero
     */
    private static final long serialVersionUID = 1L;

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name="titulo")
    private String title;
    
    @Column(name = "contenido")
    private String content;

    /**
     * h2 Estos campos permiten saber cuandos se creo el registro o se modificó
     * h3 @CreatedDate inserta la fecha en el momento que se cree la instancia
     */
    @CreatedDate
    @Column(name = "created_at", nullable = false,updatable = false)
    private Date createdAt;

    @LastModifiedDate
    @Column(name = "update_at")
    private Date updateAt;

    /**
     * * Varios documentos tienen una categoría
     * h2 Implementando una relación de entidades
     * h3 @JoinColum es un @Column pero para relaciones
     * h3 @insertable, No se insertarán categorías desde documentos
     * h3 @updatable, No puedes actulizar categorías, tienes que ir a su CRUD
     * h3 especialmente igual con @insertable
     */

    @ManyToOne
    @JoinColumn(name = "categoria_id", insertable = false, updatable = false)
    private Category category;

    @OneToMany(mappedBy = "document")
    private List<File> file;

    @OneToMany(mappedBy = "document")
    private List<UserDocument> userDocuments;
    
    
    public Document() {
	}

	public Document(String title, String content, Date createdAt, Date updateAt, Category category, List<File> file,
			List<UserDocument> userDocuments) {
		this.title = title;
		this.content = content;
		this.createdAt = createdAt;
		this.updateAt = updateAt;
		this.category = category;
		this.file = file;
		this.userDocuments = userDocuments;
	}

	
	
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public List<File> getFile() {
		return file;
	}

	public void setFile(List<File> file) {
		this.file = file;
	}

	public List<UserDocument> getUserDocuments() {
		return userDocuments;
	}

	public void setUserDocuments(List<UserDocument> userDocuments) {
		this.userDocuments = userDocuments;
	}
    
    
}
