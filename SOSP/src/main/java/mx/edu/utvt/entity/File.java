package mx.edu.utvt.entity;



import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "archivo")

public class File implements Serializable {
    /**
     * * El peso lo hace más ligero
     */
    private static final long serialVersionUID = 1L;

    @Id @GeneratedValue (strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "titulo")
    private String title;

    @Column(name = "archivo")
    @Lob
    private byte[] file;

    @Column(name = "detalle")
    private String detail;

    @ManyToOne
    @JoinColumn(name = "documento_id", insertable = false, updatable = false)
    private Document document;

    @ManyToOne
    @JoinColumn(name = "tipo_archivo_id") //, insertable = false, updatable = false
    private FileType fileType;

    public File() {
	
	}
    
    
	public File(String title, byte[] file, String detail, Document document, FileType fileType) {
		this.title = title;
		this.file = file;
		this.detail = detail;
		this.document = document;
		this.fileType = fileType;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public FileType getFileType() {
		return fileType;
	}

	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}

}
