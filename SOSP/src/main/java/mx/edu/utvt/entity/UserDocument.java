package mx.edu.utvt.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "usuario_documento")
public class UserDocument implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "estado")
    private boolean status;

    @ManyToOne
    @JoinColumn(name = "usuarios_id", insertable = false, updatable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "documento_id", insertable = false, updatable = false)
    private Document document;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

    
    
}
