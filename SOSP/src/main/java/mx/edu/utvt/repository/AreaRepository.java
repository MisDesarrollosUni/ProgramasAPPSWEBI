package mx.edu.utvt.repository;

import mx.edu.utvt.entity.Area;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AreaRepository extends JpaRepository<Area, Long> {

    /** h1 Usar el prefijo
     * * - findBy -
     * h1 y el atributo
     */
    List<Area> findByDescriptionLike(String description);

}
