package mx.edu.utvt.repository;

import mx.edu.utvt.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

	  /**
     * ? Saldaña Espinoza Hector
     * ? 5C - DSM
     *
     * * METHOD QUERIES
     * ! Concepto:
     * El módulo JPA permite definir una consulta manualmente como una cadena o derivarla del nombre del método.
     * Osea que es la forma en la que se puede realizar una consulta, pero esta presentada en
     * un método y llamándolo similar a lo que requiere en nuestra interfaz.
     *
     * ! Ejemplo:
     * Usando el término [findBy] [NombreDel Atributo] [Criterio]
     *            findBy_Type_Equals => findByTypeEquals()
     * ! @Query:
     * El uso de consultas como el ejemplo anterior es un método eficaz para una pequeña cantidad de consultas.
     * Debido a que la consulta en sí está vinculada al método Java que los ejecuta, en realidad puede vincularlos
     *  directamente usando la anotación Spring Data JPA @Query sin tener que anotarlos en la clase de dominio.
     *
     * ! Ejemplo:
     * Seria similar a ejecutar una consulta como en SQL
     * @Query("select u from User u where u.emailAddress = ?1")
     * ó
     * @Query( value = "select * from User", nativeQuery = true) de forma nativa
     */

    // ? • Buscar la categoría que coincida exactamente con el tipo de categoría.
    Category findByTypeEquals(String type);

    // ? • Buscar todas las categorías de acuerdo con el estado (true o false)
    List<Category> findAllByStatusIsTrue();
    List<Category> findAllByStatusIsFalse();

    // ? • Buscar la categoría que coincida con el tipo de categoría (like)
    Category findByTypeLike(String type);

    /**
     * * Referencias
     * Spring. (s. f.). Spring Data JPA - Reference Documentation. Docs Spring.
     * Recuperado 7 de marzo de 2021, de https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods
     *
     * DeGregorio, A. (2020, 29 julio). LIKE Queries in Spring JPA Repositories.
     * Recuperado 7 de marzo de 2021, de Baeldung. https://www.baeldung.com/spring-jpa-like-queries
     *
     */

	
}
