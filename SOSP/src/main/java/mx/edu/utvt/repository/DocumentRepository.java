package mx.edu.utvt.repository;

import mx.edu.utvt.entity.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentRepository  extends JpaRepository<Document, Long> {
}
