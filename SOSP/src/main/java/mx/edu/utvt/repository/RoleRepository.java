package mx.edu.utvt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.edu.utvt.entity.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	Role findRoleByRole(String name);
}
