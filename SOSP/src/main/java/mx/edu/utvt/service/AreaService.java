package mx.edu.utvt.service;

import mx.edu.utvt.entity.Area;
import mx.edu.utvt.repository.AreaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AreaService {

    /**
     * h1 Se le inyecta una clase a service
     */
    @Autowired
    private AreaRepository areaRepository;

    public List<Area> getAll() {
        return areaRepository.findAll();
    }

    public List<Area> filterByDescription(String criterio) {
        return areaRepository.findByDescriptionLike(criterio);
    }

    /**
     * h2 El findById(id).get ya trae el objeto
     *
     * @param id
     * @return
     */

    public Area getArea(long id) {
        return areaRepository.findById(id).get();
//        return areaRepository.getOne(id);  h1 Solo trae la referencia el memoria no a los datos
    }

    public Area saveOrUpdate(Area area) {
        return areaRepository.save(area);
    }

    public void remove(long id) {
        areaRepository.deleteById(id);
    }


}
