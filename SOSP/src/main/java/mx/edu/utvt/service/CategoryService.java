package mx.edu.utvt.service;

import mx.edu.utvt.entity.Category;
import mx.edu.utvt.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public List<Category> getAll(){
        return categoryRepository.findAll();
    }

    public Category getCategory(long id){
        return categoryRepository.findById(id).get();
    }

    public Category saveOrUpdate(Category category){
        return categoryRepository.save(category);
    }

    public void remove(long id){
        categoryRepository.deleteById(id);
    }
    
    // ? • Buscar la categoría que coincida exactamente con el tipo de categoría.
    public Category findByTypeEquals(String type) {
        return categoryRepository.findByTypeEquals(type);
    }

    // ? • Buscar todas las categorías de acuerdo con el estado (true o false)
    public List<Category> findAllByStatusIsTrue(){
        return categoryRepository.findAllByStatusIsTrue();
    }

    public List<Category> findAllByStatusIsFalse(){
        return categoryRepository.findAllByStatusIsFalse();
    }

    // ? • Buscar la categoría que coincida con el tipo de categoría (like)
    public Category findByTypeLikeOf(String type){
        String typeModified = "%"+type+"%";
        return categoryRepository.findByTypeLike(typeModified);
    }
    
}

