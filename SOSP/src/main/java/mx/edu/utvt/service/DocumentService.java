package mx.edu.utvt.service;

import mx.edu.utvt.entity.Document;
import mx.edu.utvt.repository.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;

@Service
public class DocumentService {

    @Autowired
    private DocumentRepository documentRepository;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private FileService fileService;
    
    
    public List<Document> getAll(){
        return documentRepository.findAll();
    }

    public Document getOne(long id){
        return documentRepository.findById(id).get();
    }

    public Document saveOrUpdate(Document document, long categoryId, long fileTypeId, MultipartFile file){
    	Document storeDocument = null;
    	
    	try {
    		document.setCategory(categoryService.getCategory(categoryId));    
        	document.setCreatedAt(new Date());
        	document.setUpdateAt(new Date());
        	storeDocument = documentRepository.save(document);
        	fileService.saveOrUpdate(file, fileTypeId, storeDocument);
    	}catch (Exception e) {
			e.printStackTrace();
		}	   	     	
    	return storeDocument; 	
    }

    public void remove(long id){
        documentRepository.deleteById(id);
    }
    
}
