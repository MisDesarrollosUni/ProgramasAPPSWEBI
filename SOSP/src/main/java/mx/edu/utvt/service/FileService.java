package mx.edu.utvt.service;

import mx.edu.utvt.entity.Document;
import mx.edu.utvt.entity.File;
import mx.edu.utvt.repository.FileRepository;
import mx.edu.utvt.repository.FileTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class FileService {

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private FileTypeRepository fileTypeRepository;
    
    public List<File> getAll(){
        return fileRepository.findAll();
    }

    public File getFile(long id){
        return fileRepository.findById(id).get();
    }
    
    public File saveOrUpdate(MultipartFile file, long fileTypeId, Document document) throws IOException {
    	// este método quita lo sigueitne C:/User/Carpeta/archivo.txt   y se queda colo con /archivo
    	String fileName = StringUtils.cleanPath(file.getName());
    	File fileEntity = new File();
    	fileEntity.setTitle(fileName);
    	fileEntity.setFile(file.getBytes()); // Siempre podemos caer un error al manejar archivos por eso se agrega el IOException
    	fileEntity.setDetail("Nuevo archivo");
    	fileEntity.setDocument(document);
    	//insert into archivo(tipo_archivo_id) values (5) <- Referencia
    	fileEntity.setFileType(fileTypeRepository.getOne(fileTypeId)); // getOne solo trae un proxi, osea el ID del objeto, no podemos acceder a el objeto por completo
    	return fileRepository.save(fileEntity);
    }

    public void remove(long id){
        fileRepository.deleteById(id);
    }

}
