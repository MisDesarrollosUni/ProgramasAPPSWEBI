package mx.edu.utvt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import  mx.edu.utvt.entity.Role;
import  mx.edu.utvt.repository.RoleRepository;

@Service
public class RoleService {

	@Autowired
	private RoleRepository roleRepository;
	
    public Role findByName(String name) {
        Role role = roleRepository.findRoleByRole(name);
        return role;
    }
	
}
