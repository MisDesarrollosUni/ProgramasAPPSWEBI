package mx.edu.utvt.service;

import mx.edu.utvt.entity.UserDocument;
import mx.edu.utvt.repository.UserDocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserDocumentService {

    @Autowired
    private UserDocumentRepository userDocumentRepository;

    public List<UserDocument> getAll() {
        return userDocumentRepository.findAll();
    }

    public UserDocument getUserDocument(long id) {
        return userDocumentRepository.findById(id).get();
    }

    public UserDocument saveOrUpdate(UserDocument userDocument) {
        return userDocumentRepository.save(userDocument);
    }

    public void remove(long id){
        userDocumentRepository.deleteById(id);
    }

}
